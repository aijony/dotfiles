;;; packages.el --- better-auto-completion layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2016 Sylvain Benner & Contributors
;;
;; Author:  <aidan@excli>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `better-auto-completion-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `better-auto-completion/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `better-auto-completion/pre-init-PACKAGE' and/or
;;   `better-auto-completion/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:


(setq better-auto-completion-packages '(
                                        yasnippet
                                        ))

(defun better-auto-completion/post-init-yasnippet ()
  (spacemacs/toggle-yasnippet-on)
  (add-to-list 'yas-snippet-dirs (expand-file-name "~/.spacemacs.d/snippets"))
  )
;;; packages.el ends here
